
export const enum SeverityType {
    Critical = "Critical",
    High = "High",
    Medium = "Medium",
    Low = "Low",
    Info = "Informational",
}