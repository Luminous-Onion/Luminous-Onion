export const headerHeight = 48;
export const sideNavBarWidth = 60;

export const outputDateFormat = "YYYY-MM-DD";


export const FindingSeverity = {
    Critical :  {
        abbr: "C",
        color: {
            base: "#8b0000",
            light:"#C57F7F"
        }
    },
    High: {
        abbr: "H",
        color: {
            base: "#FF8C00",
            light: "#ffc57f"
        }
    },
    Medium: {
        abbr: "M",
        color: {
            base: "#E4AB10",
            light: "#f1d587"
        }
    },
    Low: {
        abbr: "L",
        color: {
            base: "#999999",
            light: "#cccccc"
        }
    },
    Info : {
        abbr: "I",
        color : {
            base: "#CCCCCC",
            light: "#eaeaea"
        }
    }
}